package xyz.filipe.ifamqacademico;

import android.Manifest;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardArrayAdapter;
import it.gmariotti.cardslib.library.prototypes.CardSection;
import it.gmariotti.cardslib.library.view.CardListView;

public class DownloadsFragment  extends Fragment {
    static View view;
    static Context context;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_downloads, container, false);
        context = getContext();
        MainActivity.getDownloads();
        if(MainActivity.file_list==null)
            MainActivity.load_panel.setVisibility(View.VISIBLE);
        return view;
    }

    public static void loaded(){
        MainActivity.load_panel.setVisibility(View.GONE);
        ArrayList<Card> cards = new ArrayList<Card>();
        List<CardSection> sections =  new ArrayList<CardSection>();
        String lastMat = "";
        int i = 0;
        for(FileObject file : MainActivity.file_list) {
            Card card = new CardsDownloads(context, file);
            cards.add(card);
            if(lastMat!=file.materia)
                sections.add(new CardSection(i,file.materia));
            lastMat = file.materia;
            i++;
        }
        CardSection[] dummy = new CardSection[sections.size()];

        CardArrayAdapter mCardArrayAdapter = new CardArrayAdapter(context,cards);
        CardsDownloadsSectionAdapter mAdapter = new CardsDownloadsSectionAdapter(context, mCardArrayAdapter);

        mAdapter.setCardSections(sections.toArray(dummy));

        CardListView listView = (CardListView) view.findViewById(R.id.list);

        if (listView!=null){
            listView.setExternalAdapter(mAdapter,mCardArrayAdapter);
        }
    }
}