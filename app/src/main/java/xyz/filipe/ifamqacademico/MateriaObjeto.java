package xyz.filipe.ifamqacademico;

import java.io.Serializable;

public class MateriaObjeto implements Serializable {
    public String nome;
    public String professor;
    public int carga;
    public int faltas;
    public int aulas;

}
