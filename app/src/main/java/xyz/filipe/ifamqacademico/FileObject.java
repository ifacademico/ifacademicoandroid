package xyz.filipe.ifamqacademico;

import java.io.Serializable;

public class FileObject implements Serializable {
    public String materia;
    public String title;
    public String url;
    public String data;
}
