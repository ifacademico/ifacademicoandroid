package xyz.filipe.ifamqacademico;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import it.gmariotti.cardslib.library.internal.Card;

public class CardsDiario extends Card {
    MateriaObjeto mat;
    public CardsDiario(Context context) {
        this(context, R.layout.card_layout_diario);
    }
    public CardsDiario(Context context, MateriaObjeto m) {
        super(context, R.layout.card_layout_diario);
        mat = m;
        init();
    }
    public CardsDiario(Context context, int innerLayout) {
        super(context, innerLayout);
        init();
    }
    private void init(){
        setOnClickListener(new OnCardClickListener() {
            @Override
            public void onClick(Card card, View view) {
                //Toast.makeText(getContext(), "Click Listener card=", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {
        TextView mTitle = (TextView) parent.findViewById(R.id.title);
        TextView mProf = (TextView) parent.findViewById(R.id.prof);
        TextView mAulas = (TextView) parent.findViewById(R.id.aulas);
        TextView mFaltas = (TextView) parent.findViewById(R.id.faltas);
        TextView mPresenca = (TextView) parent.findViewById(R.id.presenca);
        mTitle.setText(mat.nome);
        mProf.setText(mat.professor);
        mAulas.setText(mat.aulas+"/"+mat.carga);
        String faltas = mat.faltas + " " + (mat.faltas==1?"falta":"faltas");
        mFaltas.setText(faltas);
        mPresenca.setText(100-mat.faltas*100/mat.carga + "% de presença");
    }
}