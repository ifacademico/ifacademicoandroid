package xyz.filipe.ifamqacademico;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardGridArrayAdapter;
import it.gmariotti.cardslib.library.view.CardGridView;

public class DiarioFragment  extends Fragment {
    static View view;
    static Context context;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_diario, container, false);
        context = getContext();
        MainActivity.getMaterias();
        if(MainActivity.materias_list==null)
            MainActivity.load_panel.setVisibility(View.VISIBLE);
        return view;
    }

    public static void loaded(){
        ArrayList<Card> cards = new ArrayList<Card>();
        for(MateriaObjeto mat : MainActivity.materias_list) {
            Card card = new CardsDiario(context, mat);
            cards.add(card);
        }

        CardGridArrayAdapter mCardArrayAdapter = new CardGridArrayAdapter(context,cards);

        CardGridView gridView = (CardGridView) view.findViewById(R.id.grid);
        gridView.setNumColumns(1);
        if (gridView!=null){
            gridView.setAdapter(mCardArrayAdapter);
        }
        MainActivity.load_panel.setVisibility(View.GONE);
    }
}