package xyz.filipe.ifamqacademico;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    public static WebView webview;
    public static String cookies;
    private DrawerLayout mDrawer;
    private ActionBarDrawerToggle mDrawerToggle;
    private Toolbar toolbar;
    static LinearLayout load_panel;
    private NavigationView nvDrawer;
    static View headerLayout;
    public static String IF = "IFAM";
    public static String IF_URL = "";
    public static ArrayList<MateriaObjeto> materias_list;
    public static ArrayList<FileObject> file_list;
    public static MenuItem periodo_menu_item;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        switch(IF) {
            default:
            case "IFAM":
                IF_URL = "http://academico.ifam.edu.br";
                break;
        }
        materias_list = null;
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        file_list = null;
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        nvDrawer = (NavigationView) findViewById(R.id.nvView);

        mDrawer.setDrawerLockMode(1);
        setupDrawerContent(nvDrawer);

        headerLayout = nvDrawer.inflateHeaderView(R.layout.drawer_header);

        final SharedPreferences prefs = getBaseContext().getSharedPreferences("xyz.filipe.ifamqacademico.preferences", Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = prefs.edit();
        if(prefs.getInt("prev_error", 0)!=0 || !prefs.getBoolean("auth", false)){
            Intent intent = new Intent(this, LoginActivity.class);
            finish();
            startActivity(intent);
        }
        else{
            editor.putBoolean("auth",false);
            editor.commit();
        }

        load_panel = (LinearLayout)findViewById(R.id.load_panel);

        ProgressDialog progressDialog = new ProgressDialog(this);


        webview = (WebView)findViewById(R.id.webview);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.setWebChromeClient(new WebChromeClient());
        webview.addJavascriptInterface(new JavaScriptInterface(), "HTMLOUT");
        webview.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                cookies = CookieManager.getInstance().getCookie(url);
                Log.i("URL", url);
                if(url.equals(IF_URL+"/qacademico/index.asp?t=1001")) {
                    view.evaluateJavascript("javascript:document.getElementById('txtLogin').value = '"+prefs.getString("matricula","")+"'", null);
                    view.evaluateJavascript("javascript:document.getElementById('txtSenha').value = '"+prefs.getString("senha","")+"'", null);
                    view.evaluateJavascript("javascript:document.forms['frmLogin'].submit()", null);
                }
                if(url.equals(IF_URL+"/qacademico/index.asp?t=1")){
                    editor.putInt("prev_error", 1);
                    editor.commit();
                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    finish();
                    startActivity(intent);
                }
                else view.evaluateJavascript("window.HTMLOUT.checkErro(document.getElementsByTagName('html')[0].innerHTML);", null);
                if(url.equals(IF_URL+"/qacademico/index.asp?t=2071")){
                    injectScriptFile(view, IF+"/ShowDiario.js");
                }
                if(url.equals(IF_URL+"/qacademico/index.asp?t=2061")){
                    //view.evaluateJavascript("window.HTMLOUT.showDownloads(document.getElementsByTagName('html')[0].innerHTML);", null);
                    injectScriptFile(view, IF+"/ShowDownloads.js");
                }
                if(url.equals(IF_URL+"/qacademico/index.asp?t=2000")){
                    view.evaluateJavascript("window.HTMLOUT.startPage(document.getElementsByTagName('html')[0].innerHTML);", null);
                    load_panel.setVisibility(View.GONE);
                }
            }
        });

        webview.loadUrl(IF_URL+"/qacademico/index.asp?t=1001");
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawer,
                R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
            }
        };
        mDrawer.setDrawerListener(mDrawerToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
    private void injectScriptFile(WebView view, String scriptFile) {
        InputStream input;
        try {
            input = getAssets().open(scriptFile);
            byte[] buffer = new byte[input.available()];
            input.read(buffer);
            input.close();

            // String-ify the script byte-array using BASE64 encoding !!!
            String encoded = Base64.encodeToString(buffer, Base64.NO_WRAP);
            view.loadUrl("javascript:(function() {" +
                    "var parent = document.getElementsByTagName('head').item(0);" +
                    "var script = document.createElement('script');" +
                    "script.type = 'text/javascript';" +
                    // Tell the browser to BASE64-decode the string into your script !!!
                    "script.innerHTML = window.atob('" + encoded + "');" +
                    "parent.appendChild(script)" +
                    "})()");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    class JavaScriptInterface {
        @JavascriptInterface
        public void startPage(String html) {
            Document doc = Jsoup.parse(html);
            Element title = doc.select("td[class=titulo]").get(1);

            ((TextView)headerLayout.findViewById(R.id.name)).setText(title.text());
            mDrawer.setDrawerLockMode(0);
        }

        @JavascriptInterface
        public void showDiario(String data) {

            try {
                JSONObject json = new JSONObject(data);
                JSONArray diarios = json.getJSONArray("diarios");
                ArrayList<MateriaObjeto> mats = new ArrayList<MateriaObjeto>();
                for (int i = 0; i < diarios.length(); i++) {
                    JSONObject d = diarios.getJSONObject(i);
                    JSONObject mat = d.getJSONObject("mat");
                    JSONObject fre = d.getJSONObject("frequencia");
                    Log.i("Nome", mat.getString("nome"));
                    MateriaObjeto materia = new MateriaObjeto();
                    materia.nome = mat.getString("nome");
                    materia.carga = mat.getInt("ch");
                    materia.professor = mat.getString("professor");
                    materia.aulas = fre.getInt("aulas_ministradas");
                    materia.faltas = fre.getInt("faltas");


                    mats.add(materia);
                }
                materias_list = mats;

                final ArrayList<String> al = new ArrayList<>();
                al.add("Período");
                JSONArray periodos = json.getJSONArray("periodos");
                for (int i = 0; i < periodos.length(); i++) {
                    al.add(periodos.getString(i));
                }



                final Spinner spinner = (Spinner) MenuItemCompat.getActionView(periodo_menu_item);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        DiarioFragment.loaded();
                        periodo_menu_item.setVisible(true);
                        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(getSupportActionBar().getThemedContext(),
                                android.R.layout.simple_spinner_dropdown_item, al) ;

                        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
                        {
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
                            {
                                if(position==0) return;
                                Log.i("PERIODO", al.get(position));
                                String postData = "ANO_PERIODO2="+al.get(position).replace(" / ", "_");
                                webview.postUrl(IF_URL+"/qacademico/index.asp?t=2071", postData.getBytes());
                                load_panel.setVisibility(View.VISIBLE);
                            } // to close the onItemSelected
                            public void onNothingSelected(AdapterView<?> parent)
                            {

                            }
                        });

                        spinner.setAdapter(spinnerAdapter);

                    }
                });
            }
            catch(Exception e){
                Log.e("JSON ERROR",e.getLocalizedMessage());
            }
        }

        @JavascriptInterface
        public void showDownloads(String data) {
            Log.i("html", data);

            try {
                JSONObject json = new JSONObject(data);
                Log.i("e", json.toString());
                JSONArray arquivos = json.getJSONArray("arquivos");
                ArrayList<MateriaObjeto> mats = new ArrayList<MateriaObjeto>();
                ArrayList<FileObject> fol = new ArrayList<>();
                for (int i = 0; i < arquivos.length(); i++) {
                    JSONObject d = arquivos.getJSONObject(i);
                    JSONObject mat = d.getJSONObject("mat");
                    JSONArray materiais = d.getJSONArray("materiais");
                    for (int j = 0; j < materiais.length(); j++) {
                        FileObject fo = new FileObject();
                        fo.materia = mat.getString("nome");
                        fo.data = materiais.getJSONObject(j).getString("data_publicacao");
                        fo.title = materiais.getJSONObject(j).getString("titulo");
                        fo.url = materiais.getJSONObject(j).getString("link");
                        Log.i("URL", fo.url);
                        fol.add(fo);
                    }
                }
                file_list = fol;

                final ArrayList<String> al = new ArrayList<>();
                al.add("Período");
                JSONArray periodos = json.getJSONArray("periodos");
                for (int i = 0; i < periodos.length(); i++) {
                    al.add(periodos.getString(i));
                }
                final Spinner spinner = (Spinner) MenuItemCompat.getActionView(periodo_menu_item);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        DownloadsFragment.loaded();
                        periodo_menu_item.setVisible(true);
                        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(getSupportActionBar().getThemedContext(),
                                android.R.layout.simple_spinner_dropdown_item, al);

                        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                if (position == 0) return;
                                Log.i("PERIODO", al.get(position));
                                String postData = "ANO_PERIODO=" + al.get(position).replace(" / ", "_");
                                webview.postUrl(IF_URL + "/qacademico/index.asp?t=2061", postData.getBytes());
                                load_panel.setVisibility(View.VISIBLE);
                            } // to close the onItemSelected

                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                        spinner.setAdapter(spinnerAdapter);

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @JavascriptInterface
        public void checkErro(String html) {
            if(html.toLowerCase().contains("acesso negado")) {
                final SharedPreferences prefs = getBaseContext().getSharedPreferences("xyz.filipe.ifamqacademico.preferences", Context.MODE_PRIVATE);
                final SharedPreferences.Editor editor = prefs.edit();
                editor.putInt("prev_error", 2);
                editor.commit();
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                finish();
                startActivity(intent);
            }
        }
    }
    public static void getMaterias(){
        if(materias_list!=null)DiarioFragment.loaded();
        else webview.loadUrl(IF_URL+"/qacademico/index.asp?t=2071");
    }
    public static void getDownloads(){
        webview.loadUrl(IF_URL+"/qacademico/index.asp?t=2061");
    }

    public void exit(){

        new AlertDialog.Builder(MainActivity.this)
                .setTitle("Desconectar")
                .setMessage("Tem certeza que deseja sair da sua conta?")
                .setPositiveButton("Sim", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final SharedPreferences prefs = getBaseContext().getSharedPreferences("xyz.filipe.ifamqacademico.preferences", Context.MODE_PRIVATE);
                        final SharedPreferences.Editor editor = prefs.edit();
                        editor.putBoolean("auth", false);
                        editor.putBoolean("auto_login", false);
                        editor.commit();

                        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                        finish();
                        startActivity(intent);
                    }

                })
                .setNegativeButton("Não", null)
                .show();
    }
    private void setupDrawerContent(NavigationView navigationView) {
        //navigationView.getMenu().findItem(R.id.inicio).setIcon(new IconicsDrawable(this).icon(GoogleMaterial.Icon.gmd_home).color(Color.BLACK).sizeDp(24));
        navigationView.getMenu().findItem(R.id.diario).setIcon(new IconicsDrawable(this).icon(GoogleMaterial.Icon.gmd_book).color(Color.BLACK).sizeDp(24));
        navigationView.getMenu().findItem(R.id.downloads).setIcon(new IconicsDrawable(this).icon(GoogleMaterial.Icon.gmd_folder).color(Color.BLACK).sizeDp(24));
        //navigationView.getMenu().findItem(R.id.config).setIcon(new IconicsDrawable(this).icon(GoogleMaterial.Icon.gmd_settings).color(Color.BLACK).sizeDp(24));
        navigationView.getMenu().findItem(R.id.sair).setIcon(new IconicsDrawable(this).icon(GoogleMaterial.Icon.gmd_exit_to_app).color(Color.BLACK).sizeDp(24));
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    public void selectDrawerItem(MenuItem menuItem) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                periodo_menu_item.setVisible(false);
            }
        });
        Fragment fragment = null;
        Class fragmentClass = null;
        boolean change = false;
        switch(menuItem.getItemId()) {
            //case R.id.inicio:
            //  fragmentClass = MainFragment.class;
            //  change = true;
            //  break;
            case R.id.diario:
                fragmentClass = DiarioFragment.class;
                change = true;
                break;
            case R.id.downloads:
                fragmentClass = DownloadsFragment.class;
                change = true;
                break;
            case R.id.sair:
                exit();
                break;
            default:
                fragmentClass = DiarioFragment.class;
                change = true;
        }
        if(change) {
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }

            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
            menuItem.setChecked(true);
            setTitle(menuItem.getTitle());
        }
        else menuItem.setChecked(false);
        mDrawer.closeDrawers();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawer.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.diario_menu, menu);

        periodo_menu_item = menu.findItem(R.id.miSpinner);
        periodo_menu_item.setVisible(false);
        return true;

    }
}
