package xyz.filipe.ifamqacademico;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import it.gmariotti.cardslib.library.internal.CardArrayAdapter;
import it.gmariotti.cardslib.library.prototypes.CardSection;
import it.gmariotti.cardslib.library.prototypes.SectionedCardAdapter;

public class CardsDownloadsSectionAdapter extends SectionedCardAdapter {

    public CardsDownloadsSectionAdapter(Context context, CardArrayAdapter cardArrayAdapter) {
        super(context, R.layout.sec_adapter,cardArrayAdapter);
    }

    @Override
    protected View getSectionView(int position, View view, ViewGroup parent) {
        CardSection section = (CardSection) getCardSections().get(position);

        if (section != null ) {
            //Set the title
            TextView title = (TextView) view.findViewById(R.id.title);
            if (title != null)
                title.setText(section.getTitle());
        }

        return view;
    }
}