var mats = [];

var $linhas_da_tabela = document.querySelectorAll('body > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > table:nth-child(4) > tbody > tr');

var $periodos = document.querySelectorAll('#ANO_PERIODO > option');
for( var l = 1, pr = []; l < $periodos.length; l++ ) {
	var $linha = $periodos[l].innerText;
	pr.push($linha);
}

var first_letter_uppercase_for_word = function(txt){return ( /^M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$/g.test(txt) || (txt.toLowerCase().replace("unidade","").replace("prova","").trim().toUpperCase() != "" && /^M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$/g.test(txt.toLowerCase().replace("unidade","").replace("prova","").trim().toUpperCase())) ) ? txt : (((['de', 'da', 'do', 'das', 'dos', 'os', 'em', 'nos', 'nas', 'no', 'na', 'as', 'um', 'uns', 'uma', 'a', 'e', 'o',]).indexOf(txt.toLowerCase().trim()) == -1) ? ( txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase() ) : ( txt.toLowerCase() ));}

var replace_uppercase = function(txt) { return txt.toUpperCase(); }

for( var l = 1, c = -1, mat = {}, materiais = []; l < $linhas_da_tabela.length; l++ ) {
	var $linha = $linhas_da_tabela[l];
	if( $linha.className == "rotulo" ) {
		if( l != 1 ) mats.push({ mat: mat, materiais: materiais });
		c++;
		var text = $linha.querySelector('td:nth-child(2)').innerText;
		var regex = /([0-9]+) -(.+)- (.+)\(([0-9\.]+)\)/g;
		var data = regex.exec(text);
		var id = parseInt(data[1]);
		var turma = data[2];
		var nome = data[3].replace(/\(.+\)/g, replace_uppercase);
		var ch = parseFloat(data[4]);

		mat = { id: id, nome: nome, turma: turma, ch: ch };
		materiais = [];
	} else {
		var data_de_publicacao = $linha.querySelector('td').innerText.trim();
		var content = $linha.querySelector('td:nth-child(2)');
		var titulo = content.querySelector('a').innerText.trim();
		titulo = ( /[a-z]/g.test(titulo) && /[A-Z]/g.test(titulo) && titulo.indexOf(" ") == -1 ) ? titulo : titulo.replace(/[\w\u00C0-\u017F]\S*/g, first_letter_uppercase_for_word);
		titulo = titulo.replace(/\//g, '-');
		var descricao = (content.innerText.search('Observações:') != -1) ? (/Observações\:(.+)/g).exec(content.innerText)[1].trim() : null;
		var $link_element = document.createElement('a');
		$link_element.href = content.querySelector('a').getAttribute('href').trim();
		var link = $link_element.href;

		materiais.push({ data_publicacao: data_de_publicacao, titulo: titulo, descricao: descricao, link: link });
	}

	if( l + 1 == $linhas_da_tabela.length && $linhas_da_tabela.length > 1 ) mats.push({ mat: mat, materiais: materiais });
}

var r = {};

r["arquivos"] = mats;
r["periodos"] = pr;

if(window.HTMLOUT != undefined) {
	window.HTMLOUT.showDownloads(JSON.stringify(r));
} else {
	JSON.stringify(r);
}

