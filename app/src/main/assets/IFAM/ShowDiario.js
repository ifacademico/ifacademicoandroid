var nome_periodo = document.querySelector('body > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > table:nth-child(3) > tbody > tr > td:nth-child(2) > div > span').innerText.replace('/', '.').trim();

var mats = [];

var $linhas_da_tabela = document.querySelectorAll('body > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > table:nth-child(3) > tbody > tr > td:nth-child(2) > p > table:nth-child(3) > tbody > tr');

var $periodos = document.querySelectorAll('#ANO_PERIODO2 > option');
for( var l = 1, pr = []; l < $periodos.length; l++ ) {
	var $linha = $periodos[l].innerText;
	pr.push($linha);
}

var first_letter_uppercase_for_word = function(txt){return ( /^M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$/g.test(txt) || (txt.toLowerCase().replace("unidade","").replace("prova","").trim().toUpperCase() != "" && /^M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$/g.test(txt.toLowerCase().replace("unidade","").replace("prova","").trim().toUpperCase())) ) ? txt : (((['de', 'da', 'do', 'das', 'dos', 'os', 'em', 'nos', 'nas', 'no', 'na', 'as', 'um', 'uns', 'uma', 'a', 'e', 'o',]).indexOf(txt.toLowerCase().trim()) == -1) ? ( txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase() ) : ( txt.toLowerCase() ));}

var replace_uppercase = function(txt) { return txt.toUpperCase(); }

for( var l = 1, c = -1, mat = {}, frequencia = {}, unidades = []; l < $linhas_da_tabela.length; l++ ) {
	var $linha = $linhas_da_tabela[l];
	if( $linha.className != "conteudoTexto" ) {
		if( l != 1 ) mats.push({ mat: mat, frequencia: frequencia, unidades: unidades});
		c++;
		var _text = $linha.querySelector('td:nth-child(1) > strong').innerText.trim();
		var _regex = /([0-9]+) -(.+)- (.+)\(([0-9\.]+)\)( -|)(.*)/g;
		var _data = _regex.exec(_text);
		var id = parseInt(_data[1].trim());
		var turma = _data[2].trim();
		var nome = _data[3].trim().replace(/[\w\u00C0-\u017F]\S*/g, first_letter_uppercase_for_word);
			nome = nome.replace(/\(.+\)/g, replace_uppercase);
		var ch = parseFloat(_data[4].trim());
		var professor = _data[6].trim();
		professor = ( professor == null || professor.length == 0 || /^\s*$/.test(professor) ) ? "" : professor.replace(/[\w\u00C0-\u017F]\S*/g, first_letter_uppercase_for_word);


		var $frequencia = $linha.querySelector('td:nth-child(2) > table > tbody');
		var numero_de_aulas_previstas = parseInt($frequencia.querySelector('tr:nth-child(2) > td:nth-child(2)').innerText.trim());
		var aulas_ministradas = parseInt($frequencia.querySelector('tr:nth-child(3) > td:nth-child(2) > a').innerText.trim());
		var faltas = parseInt($frequencia.querySelector('tr:nth-child(4) > td:nth-child(2)').innerText.trim());
		var presenca = $frequencia.querySelector('tr:nth-child(6) > td:nth-child(2)').innerText.trim();
		var $alerta = $frequencia.querySelector('.alerta');
		var alerta = ( $alerta !== null ) ? $alerta.innerText.substring($alerta.innerText.search("\n")).trim() : null;

		mat = { id: id, nome: nome, turma: turma, ch: ch, professor: professor };
		frequencia = { numero_de_aulas_previstas: numero_de_aulas_previstas, aulas_ministradas: aulas_ministradas, faltas: faltas, presenca: presenca, alerta: alerta };
		unidades = [];
	} else {
		var nome = $linha.querySelector('td > div').innerText.trim();
		var $content = $linha.querySelectorAll('td > table > tbody > tr');
		var conteudos = [];
		for( var i = 0; i < $content.length; i++ ) {
			var $c = $content[i];
			var _regex2 = /([0-9\/]+),(.+)/g;
			var _regex2Content = _regex2.exec($c.querySelector('td:nth-child(2)').innerText.trim());
			var titulo = _regex2Content[2].trim().replace(/[\w\u00C0-\u017F]\S*/g, first_letter_uppercase_for_word);
			var tipo = titulo.substring(0, titulo.indexOf(':'));
			titulo = titulo.substring(titulo.indexOf(':') + 1).trim();
			titulo = ( /[a-z]/g.test(titulo) && /[A-Z]/g.test(titulo) && titulo.indexOf(" ") == -1 ) ? titulo : titulo.replace(/[\w\u00C0-\u017F]\S*/g, first_letter_uppercase_for_word);
			
			var data = _regex2Content[1].trim();
			var peso = parseFloat($c.querySelector('td:nth-child(3)').innerText.trim().substring(5));
			var nota_maxima = parseFloat($c.querySelector('td:nth-child(4)').innerText.trim().substring(13));
			var nota = parseFloat($c.querySelector('td:nth-child(4)').innerText.trim().substring(6));

			conteudos.push( { titulo: titulo, tipo: tipo, data: data, peso: peso, nota_maxima: nota_maxima, nota: nota } );
			
		}

		unidades.push({ nome: nome, conteudos: conteudos });
	}

	if( l + 1 == $linhas_da_tabela.length && $linhas_da_tabela.length > 1 ) mats.push({ mat: mat, frequencia: frequencia, unidades: unidades});
}

var r = {};

r["diarios"] = mats;
r["periodo_name"] = nome_periodo;
r["periodos"] = pr;

if(window.HTMLOUT != undefined) {
	window.HTMLOUT.showDiario(JSON.stringify(r));
} else {
	JSON.stringify(r);	
}

